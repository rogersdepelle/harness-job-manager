from rest_framework import serializers
from jobs.models import Job, Skill


class SkillStringRelatedField(serializers.StringRelatedField):
    def to_representation(self, value):
        return value.name

    def to_internal_value(self, data):
        obj, _ = Skill.objects.get_or_create(name=data)
        return obj


class JobSerializer(serializers.ModelSerializer):
    required_skills = SkillStringRelatedField(many=True)

    class Meta:
        model = Job
        fields = ("id", "title", "description", "required_skills")


class SkillSerializer(serializers.ModelSerializer):
    name = serializers.CharField()

    class Meta:
        model = Skill
        fields = ("name",)


class SkillCountSerializer(serializers.Serializer):
    name = serializers.CharField()
    job_count = serializers.IntegerField()
