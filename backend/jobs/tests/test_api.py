from django.urls import reverse

from rest_framework import status
from rest_framework.test import APITestCase

from jobs.models import Job, Skill
from jobs.serializers import JobSerializer, SkillCountSerializer


class JobsAPITestCase(APITestCase):
    def setUp(self):
        skill1 = Skill.objects.create(name="skill 1")
        skill2 = Skill.objects.create(name="skill 2")
        skill3 = Skill.objects.create(name="skill 3")
        job1 = Job.objects.create(title="Job 1", description="Job Desc 1")
        job1.required_skills.set([skill1, skill2])
        job2 = Job.objects.create(title="Job 3", description="Job Desc 2")
        job2.required_skills.set([skill1, skill3])
        Job.objects.create(title="Job 3", description="Job Desc 3")
        self.job_id = job1.id

    def test_get_all_jobs(self):
        response = self.client.get(reverse("job-list"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        jobs = Job.objects.all()
        serializer = JobSerializer(jobs, many=True)
        self.assertEqual(response.data["results"], serializer.data)

    def test_get_job(self):
        response = self.client.get(reverse("job-detail", kwargs={"pk": self.job_id}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        job = Job.objects.get(id=self.job_id)
        serializer = JobSerializer(job)
        self.assertEqual(response.data, serializer.data)

    def test_create_job(self):
        data = {
            "title": "Job 0",
            "description": "Job Desc 0",
            "required_skills": ["skill 0", "skill 1"],
        }
        response = self.client.post(reverse("job-list"), data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        job = Job.objects.get(title="Job 0")
        serializer = JobSerializer(job)
        self.assertEqual(response.data, serializer.data)

    def test_get_all_skills(self):
        response = self.client.get(reverse("skills-list"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        excepted_data = [
            {
                "name": "skill 1",
            },
            {
                "name": "skill 2",
            },
            {
                "name": "skill 3",
            },
        ]
        self.assertEqual(response.data["results"], excepted_data)

    def test_most_used_skills(self):
        response = self.client.get(reverse("skills-most-used-list"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        excepted_data = [
            {"name": "skill 1", "job_count": 2},
            {"name": "skill 2", "job_count": 1},
            {"name": "skill 3", "job_count": 1},
        ]
        serializer = SkillCountSerializer(excepted_data, many=True)
        self.assertEqual(response.data, serializer.data)
