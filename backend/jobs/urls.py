from django.urls import include, path
from rest_framework import routers

from jobs import views

router = routers.DefaultRouter()
router.register(r"skills/most-used", views.MostUsedSkillsViewSet, basename="skills-most-used")
router.register(r"skills", views.SkillViewSet, basename="skills")
router.register(r"jobs", views.JobViewSet)

urlpatterns = [
    path("", include(router.urls)),
]
