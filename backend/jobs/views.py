from django.db.models import Count
from rest_framework import viewsets
from rest_framework.response import Response

from jobs.models import Job, Skill
from jobs.serializers import JobSerializer, SkillSerializer, SkillCountSerializer


class JobViewSet(viewsets.ModelViewSet):
    queryset = Job.objects.all()
    serializer_class = JobSerializer
    http_method_names = ["get", "post"]


class SkillViewSet(viewsets.ModelViewSet):
    queryset = Skill.objects.all()
    serializer_class = SkillSerializer
    http_method_names = ["get"]


class MostUsedSkillsViewSet(viewsets.ViewSet):
    def list(self, request):
        queryset = (
            Skill.objects.all().annotate(job_count=Count("job")).order_by("-job_count", "name")
        )
        serializer = SkillCountSerializer(queryset, many=True)
        return Response(serializer.data)
