from django.contrib import admin

from jobs.models import Job, Skill

admin.site.register(Job)
admin.site.register(Skill)
