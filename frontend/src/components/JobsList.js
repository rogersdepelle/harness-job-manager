import React, { useState, useEffect } from "react";
import JobDataService from "../services/JobService";
import SkillDataService from "../services/SkillService";

const JobsList = () => {
  const [jobs, setJobs] = useState([]);
  const [skills, setSkills] = useState([]);
  const [currentJob, setCurrentJob] = useState(null);
  const [currentIndex, setCurrentIndex] = useState(-1);

  useEffect(() => {
    retrieveJobs();
    retrieveSkills();
  }, []);

  const retrieveSkills = () => {
    SkillDataService.getMostUsed()
      .then(response => {
        setSkills(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  };

  const retrieveJobs = () => {
    JobDataService.getAll()
      .then(response => {
        setJobs(response.data["results"]);
      })
      .catch(e => {
        console.log(e);
      });
  };

  const setActiveJob = (job, index) => {
    setCurrentJob(job);
    setCurrentIndex(index);
  };

  return (
    <div className="row pt-3">
        <div className="col-lg-4">
        <h4>Most Used Skills</h4>
        <ul>
          {skills &&
            skills.map((skill, index) => (
              <li>{skill.name} : {skill.job_count}</li>
            ))}
        </ul>

      </div>
      <div className="col-lg-4">
        <h4>Jobs List</h4>
        <ul className="list-group">
          {jobs &&
            jobs.map((job, index) => (
              <li
                className={
                  "list-group-item " + (index === currentIndex ? "active" : "")
                }
                onClick={() => setActiveJob(job, index)}
                key={index}
              >
                {job.title}
              </li>
            ))}
        </ul>

      </div>
      <div className="col-lg-4">
        {currentJob ? (
          <div>
            <h4>Job</h4>
            <div>
              <h3>
                {currentJob.title}
              </h3>
            </div>
            <div>
              <i>
                {currentJob.required_skills.join(', ')}
              </i>
            </div>
            <div className="pt-2">
              <label>
                <strong>Description:</strong>
              </label>{" "}
              {currentJob.description}
            </div>
          </div>
        ) : (
          <div>
            <br />
            <p>Please click on a Job...</p>
          </div>
        )}
      </div>
    </div>
  );
};

export default JobsList;
