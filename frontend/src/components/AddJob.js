import React, { useState, useEffect} from "react";
import JobDataService from "../services/JobService";
import SkillService from "../services/SkillService";
import CreatableSelect from 'react-select/creatable';

const AddJob = () => {
  const initialJobState = {
    id: null,
    title: "",
    description: "",
    required_skills: [],
  };
  const [job, setJob] = useState(initialJobState);
  const [skills, setSkills] = useState([]);
  const [submitted, setSubmitted] = useState(false);

  useEffect(() => {
    retrieveSkills();
  }, []);

  const retrieveSkills = () => {
    SkillService.getAll()
      .then(response => {
        var skills = response.data["results"].map(x => ({ value: x.name, label: x.name }));
        setSkills(skills);
      })
      .catch(e => {
        console.log(e);
    });
  };

  const handleInputChange = event => {
    const { name, value } = event.target;
    setJob({ ...job, [name]: value });
  };

  const handleSelectChange = skills => {
    let required_skills = skills.map(x => x.value);
    setJob({ ...job, "required_skills": required_skills });
  };

  const saveJob = () => {
    var data = {
      title: job.title,
      description: job.description,
      required_skills: job.required_skills
    };

    JobDataService.create(data)
      .then(response => {
        setJob({
          id: response.data.id,
          title: response.data.title,
          description: response.data.description,
          required_skills: response.data.required_skills,
        });
        setSubmitted(true);
      })
      .catch(e => {
        var fields = e.response.data;
        var msg = "";
        for (var field in fields){
            msg += field + ": " + fields[field][0] + "\n";
        }
        alert(msg);
      });
  };

  const newJob = () => {
    setJob(initialJobState);
    setSubmitted(false);
  };

  return (
    <div className="submit-form">
      {submitted ? (
        <div>
          <h4>You submitted successfully!</h4>
          <button className="btn btn-success" onClick={newJob}>
            Add
          </button>
        </div>
      ) : (
        <div>
          <div className="form-group pt-2">
            <label htmlFor="title">Title</label>
            <input
              type="text"
              className="form-control"
              id="title"
              required
              value={job.title}
              onChange={handleInputChange}
              name="title"
            />
          </div>

          <div className="form-group pt-2">
            <label htmlFor="description">Description</label>
            <textarea
              className="form-control"
              id="description"
              required
              value={job.description}
              onChange={handleInputChange}
              name="description"
            />
          </div>

          <div className="form-group pt-2">
            <label htmlFor="required_skills">Required Skills</label>
            <CreatableSelect
              isMulti
              options={skills}
              name="required_skills"
              onChange={handleSelectChange}
            />
          </div>

          <button onClick={saveJob} className="btn btn-success mt-3">
            Submit
          </button>
        </div>
      )}
    </div>
  );
};
export default AddJob;
