import http from "../http-common";

const getAll = () => {
  return http.get("/jobs/");
};

const get = id => {
  return http.get(`/jobs/${id}`);
};

const create = data => {
  return http.post("/jobs/", data);
};

// eslint-disable-next-line
export default {
    getAll,
    get,
    create,
  };
