import http from "../http-common";

const getAll = () => {
  return http.get("/skills/");
};

const getMostUsed = () => {
  return http.get("/skills/most-used");
};

// eslint-disable-next-line
export default {
    getAll,
    getMostUsed,
  };
