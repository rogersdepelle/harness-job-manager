# Harness Job Manager


**Local Environment**

Requirements: [Docker](https://docs.docker.com/engine/install/ubuntu/#install-using-the-convenience-script) [Docker Compose](https://docs.docker.com/compose/install/)

Run locale
```
docker-compose up
```

Load Data
```
docker-compose exec backend python manage.py loaddata core/fixture/dummy-database.json
```

API tests
```
docker-compose exec backend python manage.py test
```

* Django Admin: [127.0.0.1:8000/admin](http://127.0.0.1:8000/admin)
* React App: [127.0.0.1:3000](http://127.0.0.1:3000/)
* Django Admin Credentials: `root:toor123`

Pre-commit Install
```
virtualenv venv
source venv/bin/activate
pip install -r django/core/requirements/dev.txt
pre-commit install
```

API docs: [swaggerhub](https://app.swaggerhub.com/apis-docs/rogerspelle/harness-job-manager/1.0.0)
